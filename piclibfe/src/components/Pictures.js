import React, { useState } from 'react';

import Picture from './Picture';
import AddPictureForm from './AddPictureForm'

function Pictures () {
	let [pictures, setPictures] = useState([]);

	if (pictures.length == 0) {
		fetch ("http://localhost:8080/")
		.then ( function (res) {
			return res.json();
		})
		.then (function (data) {
			console.log(data);
			setPictures (data);
		})
	}

	let displayPictures = () => 
		pictures.map( pic =>
			<Picture key = {pic.name} pic = {pic} />
			); 


	let deletePic = name => {
		let temp = pictures.filter ( pic => pic.name!=name);
		setPictures(temp);
	}

	let addPicture = pic => {
		// let temp = {
		// 	name: name,
		// 	dateUploaded: Date.now(),
		// 	username: "Pat",
		// 	isFavorite: false
		// }
		pic.dateUploaded = pic.dateUploaded.toISOString().substring(0,10);
		setPictures([...pictures, pic]);
	}



	return (
		<div className = "row mt-5">
		<AddPictureForm  addPicture = {addPicture}/>
		{displayPictures()}
		</div>
	);
}

export default Pictures;