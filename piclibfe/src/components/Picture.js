import React from 'react';
import logo from '../logo.svg';
function Picture (props) {

	let deleteClickHandler = () => {
		fetch("http://localhost:8080/pics/delete/"+ props.pic.name, {
			method: 'delete'
		});

		props.deletePic(props.pic.name);
	}

	// Create Retrieve Update Delete
	
	// get
	// post
	// delete
	// put/patch


	return (
			<div className = "card col-md-3">
				<img className = "card-img-top" src = {logo} alt ="Card image"  />
				<div className = "card-img-overlay">
					<h4 className="card-title">
						{props.pic.name}
					</h4>
				<p className = "card-text">
				{props.pic.dateUploaded}
				</p>
				<p className ="card-text">
				{props.pic.username}
			    </p>
				<button onClick = {deleteClickHandler} className = "btn btn-danger">Delete</button>
			</div>
		</div>

		);
}

export default Picture;