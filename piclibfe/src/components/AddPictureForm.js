import React, { useState} from 'react';

function AddPictureForm (props) {
	let [name, setName] = useState ("");

	let nameChangeHandler = e => {
		setName(e.target.value);
	}

	let addBtnClickHandler = () => {
			let temp = {
				name: name,
				dateUploaded: new Date(),
				username: "Pat",
				isFavorite: false
		}

		fetch ("http://localhost:8080/pics", {
			method: 'post',
			headers: {
				'Content-Type': 'application/json'
			},
			body : JSON.stringify(temp)
		});

		props.addPicture(temp);
	}

	return (
		<form className = "col-12 mb-5" autoComplete = "off">
			Name: <input
				onChange = {nameChangeHandler}
				type = "text"
				id = "name"
				placeholder = "name"
				value = {name}

			/>

			<button onClick = {addBtnClickHandler} type = "button" className= "btn btn-success">
			Add Picture
			</button>
		</form>
		);
};

export default AddPictureForm;