import React from 'react';
import {BrowserRouter, Link, Route } from 'react-router-dom';
// import logo from './logo.svg';
import './App.css';

import Pictures from './components/Pictures.js';

import AddPictureForm from './components/AddPictureForm';
function App() {
  let name ="Patrick" ;
    return (
    <BrowserRouter>
    <Link to ="/home">Home</Link>
    <div className="App container-fluid p-5">
    	<Route path = "/" exact component = {Pictures} />
      </div>
     </BrowserRouter>
  );
}

export default App;
