package com.zuitt.demo.controllers;

import com.zuitt.demo.models.Category;
import com.zuitt.demo.models.Product;
import com.zuitt.demo.repositories.CategoryRepository;
import com.zuitt.demo.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
@CrossOrigin(origins = "http://localhost:3000")
public class ProductController {
    @Autowired
    ProductRepository productRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @GetMapping("/")
    public Iterable<Product> getProducts() {
        return productRepository.findAll();
    }

    @GetMapping("/{id}")
    public Product getProductById(@PathVariable Integer id) {
        return productRepository.findById(id).get();
    }

    @PostMapping("/{category_id}")
    public Product addProduct(@RequestBody Product product, @PathVariable Integer category_id) {
        Category category = categoryRepository.findById(category_id).get();
        product.setCategory(category);
        return productRepository.save(product);
    }

    @DeleteMapping("/products/{id}")
    public void deleteProduct(@PathVariable Integer id) {
        productRepository.deleteById(id);
    }
}