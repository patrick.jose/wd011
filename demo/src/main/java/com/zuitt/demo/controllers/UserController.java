package com.zuitt.demo.controllers;

import com.zuitt.demo.models.User;
import com.zuitt.demo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    UserRepository userRepository;

    @PostMapping("/register")
    public User registerUser(@RequestBody User user) {
        String hashedpw = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());
        user.setPassword(hashedpw);
        return userRepository.save(user);
    }

    @PostMapping("/login")
    public User loginUer(@RequestBody User user) {
        User foundUser = userRepository.findByUsername(user.getUsername());
        if(foundUser != null &&  BCrypt.checkpw(user.getPassword(), foundUser.getPassword())) {

            return foundUser;
        }
        return null;
    }
}