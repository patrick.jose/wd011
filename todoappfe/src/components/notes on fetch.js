

get - read/retrieve data from backend 

@GetMapping ("/tasks")
public something () {return all item}

fetch ("htp://localhost:8080/tasks")
.then ( res => res.json () )
.then ( data =>  {} )


post - create/add data (request body)

@PostMapping ("/tasks")
public something (@RequestBody Task task) {
	create a new task
}

fetch ("htp://localhost:8080/tasks") {
	method: 'post',
	headers: { 'Content-Type : 'application/json' },
	body.JSON.stringify(object)
}




put - edit/update (id/identifier does not change with status, request body)
@PutMapping ("/tasks/{id}")
public something (@PathVariable String id, @RequestBody //string/int/Task Task task) {
	find item by id, then update
	// item.setStatus (status);
}


let temp = {
	name: "new name"
	status: "pending"
}

fetch ("/tasks/1", { 
	method: 'put',
	headers: { 'Content-Type': 'application/json' }
	body: JSON.stringify(temp) //{name: "new name", status: }

});
fetch ("htp://localhost:8080/tasks",)

fetch


put/patch

put whole object

patch part of object only

// delete - destroy (id)

@DeleteMapping ("/tasks/{id}/delete")
public something (@PathVariable String id) {
	find by id then destroy
}



fetch ("url") // get method
let temp = {
	name: "new name"
	status: "pending"
}

fetch ("/tasks/1/delete", { //object
	method: 'delete',
	headers: { 'Content-Type': 'application/json' }
	body: JSON.stringify(temp) //{name: "new name", status: }

fetch ("/tasks/1/delete", { //object
	method: 'delete',
	body: "hello" //request body (string)
	// method: 'put',
	// body: "done" //request body (status)
})

.then ( function (res) {return res.json})
// .then ( (res ) => res.json())
// .then ( (data) => {
	//do what you want with the data
//});

.then ( function (data) {})
