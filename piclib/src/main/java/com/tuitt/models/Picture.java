package com.tuitt.models;
import java.time.LocalDate;
import java.util.UUID;

public class Picture {
    private String id = UUID.randomUUID().toString();
    private String name;
    private LocalDate dateUploaded;
    private String username;
    private boolean isFavorite;

    public Picture () {}

    public Picture(String name, LocalDate dateUploaded, String username, boolean isFavorite) {
        this.name = name;
        this.dateUploaded = dateUploaded;
        this.username = username;
        this.isFavorite = isFavorite;
    }

    // GETTERS
    public String getName() {
        return name;
    }

    public LocalDate getDateUploaded() {
        return dateUploaded;
    }

    public String getUsername() {
        return username;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    // SETTERS
    public void setName(String name) {
        this.name = name;
    }

    public void setDateUploaded(LocalDate dateUploaded) {
        this.dateUploaded = dateUploaded;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

}
