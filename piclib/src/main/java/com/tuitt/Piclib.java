package com.tuitt;

        import org.springframework.boot.SpringApplication;
        import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
        import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@ComponentScan
public class Piclib {
    public static void main(String[] args) {
        SpringApplication.run(Piclib.class, args);
    }
}