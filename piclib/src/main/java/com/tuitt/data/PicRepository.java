package com.tuitt.data;
import com.tuitt.models.Picture;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class PicRepository {
    private static Picture a = new Picture("easter-egg", LocalDate.of(2019, 4, 21), "Daisy", true);
    private static Picture b = new Picture("guitar", LocalDate.of(2019, 4, 21), "Leo", false);
    private static Picture c = new Picture("backpack", LocalDate.of(2019, 4, 21), "Marco", true);
    private static Picture d = new Picture("garden", LocalDate.of(2019, 4, 21), "Sonia", false);

    //    private static final List<Picture> ALL_PICS = Arrays.asList(a,b,c,d);
    private static List<Picture> ALL_PICS = new ArrayList<Picture>() {{
        add(a);
        add(b);
        add(c);
        add(d);
    }};

    public static List<Picture> getAllPics() {
        return ALL_PICS;
    }

    public static Picture findByName(String name) {
        for (Picture picture : ALL_PICS) {
            if (picture.getName().equals(name)) {
                return picture;
            }
        }
        return null;

    }

    public static Picture deleteByName(String name) {
        Picture temp = null;
        for (int i = 0; i < ALL_PICS.size(); i++) {
            if (ALL_PICS.get(i).getName().equals(name)) {
                temp = ALL_PICS.get(i);
                ALL_PICS.remove(i);
            }
        }
        return temp;
    }

    public static Picture addPicture (Picture picture) {
        ALL_PICS.add(picture);
        return picture;
    }
}

