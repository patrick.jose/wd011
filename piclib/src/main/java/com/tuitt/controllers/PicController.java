package com.tuitt.controllers;
import com.tuitt.data.PicRepository;
import com.tuitt.models.Picture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@ResponseBody
@CrossOrigin(origins = "http://localhost:3000")
public class PicController {
    @Autowired
    PicRepository picRepository;

    @GetMapping("/")

    public List<Picture> getAllPics() {
        return PicRepository.getAllPics();
    }

    @GetMapping("/pics/{name}")

    public List<Picture> findByName(@PathVariable String name) {
        return (List<Picture>) picRepository.findByName(name);

    }

    @DeleteMapping("/pics/delete/{name}")
    public Picture deleteAPic (@PathVariable String name) {
        return picRepository.deleteByName(name);
    }

    @PostMapping ("/pics")
    public Picture addPicture (@RequestBody Picture newPic) {
        return picRepository.addPicture(newPic);


    }
//    public ResponseEntity getAllPics() {
//
////        return ResponseEntity.ok().body(picRepository.getAllPics());
//    }
}


