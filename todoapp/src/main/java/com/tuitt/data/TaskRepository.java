package com.tuitt.data;


import com.tuitt.models.Task;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class TaskRepository {

    private static List<Task> allTasks=new ArrayList<Task>() {{
        //       Task a= new Task("eat");
        //       allTasks.add(a);
        //       Task b= new Task("code");
        //       allTasks.add(b);
        //       Task c= new Task("sleep");
        //       allTasks.add(c);
        add(new Task("eat"));
        add(new Task("Code"));
        add(new Task("Sleep"));

    }};

    public static List<Task> getAllTasks() {
        return allTasks;
    }

    public static  Task findByTask (String Task){
        for (Task a: allTasks){
            if(a.getName().equals(Task)){
                return  a;
            }
        }
        return  null;
    }
    public static  Task findById (UUID id){
        for (Task a: allTasks){
            if(a.getId().equals(id)){
                return  a;
            }
        }
        return  null;
    }



    public static  Task addTask (Task task){
        allTasks.add(task);
        return task;
    }

    public static  Task deleteTask(String id){
        Task temp = null;
        for (int i=0; i <allTasks.size(); i++){
            if (allTasks.get(i).getId().toString().equals(id)){
                temp=allTasks.get(i);
                allTasks.remove(i);
            }
        }
        return temp;
    }

//        public static Task editByName(UUID id, String name) {
//            for (int i=0; i<allTasks.size(); i++) {
//                if (allTasks.get(i).getId().equals(id)) {
//                    allTasks.get(i).setName(name);
//                    return allTasks.get(i);
//                }
//            }
//            return null;
//        }
}
//package com.tuitt.data;
//
//import com.tuitt.models.Task;
//import org.springframework.stereotype.Component;
//;
//
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Component
//public class TaskRepository {
//
//    private static List<Task> allTasks = new ArrayList<Task>() {{
//        add(new Task("eat"));
//        add(new Task("code"));
//        add(new Task("sleep"));
//    }};
//
//    public static List<Task> getAllTasks() {
//        return allTasks;
//    }
//
//    public static Task findByName(String name) {
//        for(Task task : allTasks) {
//            if(task.getName().equals(name)){
//                return task;
//            }
//        }
//        return null;
//    }
//    public static Task deleteByName(String name){
//        Task temp = null;
//        for(int i=0; i<allTasks.size(); i++) {
//            if(allTasks.get(i).getName().equals(name)) {
//                temp = allTasks.get(i);
//                allTasks.remove(i);
//            }
//        }
//        return temp;
//    }
//
//    public static Task addTask(Task task) {
//        allTasks.add(task);
//        return task;
//    }
//
}