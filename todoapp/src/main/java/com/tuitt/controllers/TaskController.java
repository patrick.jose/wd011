package com.tuitt.controllers;

import com.tuitt.data.TaskRepository;
import com.tuitt.models.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Controller
@ResponseBody
@CrossOrigin(origins = "http://localhost:3000")
public class TaskController {
    @Autowired
    TaskRepository taskRepository;

    @GetMapping("/")
    public List<Task> getAllTask(){
        return  TaskRepository.getAllTasks();
    }

    @GetMapping("/task/{name}")
    public Task findByTask(@PathVariable String name){
        return  TaskRepository.findByTask(name);
    }

    @PostMapping("/task")
    public Task addTask(@RequestBody Task newtask){
        return  TaskRepository.addTask(newtask);
    }

    @DeleteMapping("/task/delete/{name}")
    public Task deletetask(@PathVariable String name)
    {
        return TaskRepository.deleteTask(name);
    }

    @PutMapping("/task/edit/{id}")
    public Task editTask(@PathVariable UUID id, @RequestBody String name) {
        Task task= taskRepository.findById(id);
        task.setName(name);
        return task;
    }

    @PutMapping("/tasks/changeStatus/{id}")
    public Task changeStatus(@PathVariable UUID id, @RequestBody String status) {
        Task task = taskRepository.findById(id);
        task.setStatus(status);
        return task;
    }




}
//package com.tuitt.controllers;
//
//import com.tuitt.data.TaskRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.GetMapping;
//
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//import java.util.List;
//import com.tuitt.models.Task;
//@Controller
//@ResponseBody
//@CrossOrigin(origins = "http://localhost:3000")
//public class TaskController {
//    @Autowired
//    TaskRepository taskRepository;
//
//    @GetMapping("/")
//    public List<Task> getAllTasks() {
//        return taskRepository.getAllTasks();
//    }
//
//    @PostMapping("/task")
//    public Task addTask();
//
//
//}