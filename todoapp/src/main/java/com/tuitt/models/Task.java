package com.tuitt.models;

import java.util.UUID;

public class Task {
    private UUID id = UUID.randomUUID();
    private  String name;
    private  String status = "Pending";

    public Task() { }
    public Task(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UUID getId() {
        return id;
    }


}
//package com.tuitt.models;
//
//import java.util.UUID;
//
//public class Task {
//    private UUID id = UUID.randomUUID();
//    private String name;
//    private String status = "Pending";
//
//    public Task() {}
//
//    public Task(String name) {
//        this.name = name;
//    }
//
//    /* GETTERS */
//    public UUID getId() {
//        return id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public String getStatus() {
//        return status;
//    }
//
//    /* SETTERS */
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//}